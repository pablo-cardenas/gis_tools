from typing import List
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class Rerouter(Base):
    __tablename__ = "rerouter"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    edges: List["EdgeRerouter"] = relationship("EdgeRerouter",
                                               back_populates="rerouter")

    closing_routes: List["ClosingRoute"] = relationship(
        "ClosingRoute", back_populates="rerouter")
    scenarios: "RerouterScenario" = relationship("RerouterScenario",
                                                 back_populates="rerouter")


class ClosingRoute(Base):
    __tablename__ = "closing_route"

    id = Column(Integer, primary_key=True)
    rerouter_id = Column(Integer, ForeignKey("rerouter.id"))
    closing_route = Column(String)

    rerouter: "Rerouter" = relationship("Rerouter",
                                        back_populates="closing_routes")


class EdgeRerouter(Base):
    __tablename__ = "edge_rerouter"

    id = Column(Integer, primary_key=True)
    edge = Column(String)
    rerouter_id = Column(Integer, ForeignKey("rerouter.id"))

    rerouter: "Rerouter" = relationship("Rerouter", back_populates="edges")


class Scenario(Base):
    __tablename__ = "scenario"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    rerouters: "RerouterScenario" = relationship("RerouterScenario",
                                                 back_populates="scenario")


class RerouterScenario(Base):
    __tablename__ = "rerouter_scenario"

    id = Column(Integer, primary_key=True)
    scenario_id = Column(Integer, ForeignKey("scenario.id"))
    rerouter_id = Column(Integer, ForeignKey("rerouter.id"))

    rerouter: "Rerouter" = relationship("Rerouter", back_populates="scenarios")
    scenario: "Scenario" = relationship("Scenario", back_populates="rerouters")


class Interval(Base):
    __tablename__ = "interval"

    id = Column(Integer, primary_key=True)
    begin = Column(Integer)
    end = Column(Integer)
