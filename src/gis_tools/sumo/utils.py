from typing import Tuple, Iterator, Dict, Any
import itertools
import os
import subprocess
import sys
import xml.etree.ElementTree as ET

from fiona.collection import Collection
from scipy.io import hb_write
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import eigs
from scipy.spatial import KDTree
import fiona
import numpy as np
import numpy.typing as npt
import openmatrix as omx
import pandas as pd
import shapely.geometry
import sumolib


def network_schema(link_schema: Dict[str, Any]) -> Dict[str, Any]:
    schema = link_schema.copy()
    schema['properties'].update({
        'REF_IN_ID': 'str',
        'NREF_IN_ID': 'str',
        'LINK_ID': 'str',
        'DIR_TRAVEL': 'str',
    })

    return schema


DIR_TRAVEL_DICT = {
    0: 'B',
    1: 'F',
    -1: 'T',
}


def get_edge_set(
    links_collection: Collection, nodes_collection: Collection
) -> Iterator[Tuple[Dict[str, Any], Tuple[Dict[str, Any], Dict[str, Any]]]]:
    nodes_coordinates = [
        node_rec['geometry']['coordinates'] for node_rec in nodes_collection
    ]

    kdtree = KDTree(nodes_coordinates)

    for link_rec in links_collection:
        begin_point = link_rec['geometry']['coordinates'][0]
        end_point = link_rec['geometry']['coordinates'][-1]

        _, begin_index = kdtree.query(begin_point)
        _, end_index = kdtree.query(end_point)

        begin_rec = nodes_collection[int(begin_index)]
        end_rec = nodes_collection[int(end_index)]

        yield link_rec, (begin_rec, end_rec)


def get_adjacency_matrix(
    edge_set: Iterator[Tuple[Dict[str, Any], Tuple[Dict[str, Any],
                                                   Dict[str, Any]]]],
    weight_field: str,
    size: int,
    is_dual: bool,
) -> csr_matrix:
    row = []
    col = []
    data = []

    if is_dual:
        for link_rec, (begin_rec, end_rec) in edge_set:
            link_dir = link_rec['properties']['DIR']
            AB_weight = link_rec['properties']['AB_' + weight_field]
            BA_weight = link_rec['properties']['BA_' + weight_field]
            if link_dir == 0:
                if AB_weight is not None:
                    row.append(int(begin_rec['id']))
                    col.append(int(end_rec['id']))
                    data.append(float(AB_weight))

                if BA_weight is not None:
                    row.append(int(end_rec['id']))
                    col.append(int(begin_rec['id']))
                    data.append(float(BA_weight))
            elif link_dir == 1:
                if AB_weight is not None:
                    row.append(int(begin_rec['id']))
                    col.append(int(end_rec['id']))
                    data.append(float(AB_weight))
            elif link_dir == -1:
                if AB_weight is not None:
                    row.append(int(end_rec['id']))
                    col.append(int(begin_rec['id']))
                    data.append(float(AB_weight))
    else:
        for link_rec, (begin_rec, end_rec) in edge_set:
            link_dir = link_rec['properties']['DIR']
            weight = link_rec['properties'][weight_field]
            if link_dir == 0:
                if weight is not None:
                    row.append(int(begin_rec['id']))
                    col.append(int(end_rec['id']))
                    data.append(float(weight))

                    row.append(int(end_rec['id']))
                    col.append(int(begin_rec['id']))
                    data.append(float(weight))

            elif link_dir == 1:
                if weight is not None:
                    row.append(int(begin_rec['id']))
                    col.append(int(end_rec['id']))
                    data.append(float(weight))
            elif link_dir == -1:
                if weight is not None:
                    row.append(int(end_rec['id']))
                    col.append(int(begin_rec['id']))
                    data.append(float(weight))

    return csr_matrix((data, (row, col)), shape=(size, size))


def write_nodes_with_perron(
    links: Collection,
    nodes: Collection,
    weight_field: str,
    num_eigenvectors: int,
    is_dual: bool,
    output: str,
    output_eigenvalues: str,
) -> None:
    with fiona.open(nodes) as nodes_c:
        # Create new schema
        schema = nodes_c.schema.copy()
        schema['properties'].update(
            {f'PERRON{j:04d}': 'float'
             for j in range(num_eigenvectors)}, )

        # Create new meta
        meta = nodes_c.meta.copy()
        meta['schema'] = schema
        meta['driver'] = nodes_c.driver

    with fiona.open(output, 'w', **meta) as output_c,\
            fiona.open(nodes) as nodes_c,\
            fiona.open(links) as links_c:
        edge_set = get_edge_set(links_c, nodes_c)
        size = len(nodes_c)
        np.log(size)
        matrix = get_adjacency_matrix(edge_set, weight_field, size, is_dual)
        val, vec = eigs(matrix, k=num_eigenvectors, maxiter=5000, which='LM')

        np.savetxt(output_eigenvalues, val)  # type: ignore

        records = []
        for i, node_rec in enumerate(nodes_c):
            rec = node_rec.copy()
            for j in range(num_eigenvectors):
                rec['properties'][f'PERRON{j:04d}'] = vec[i, j].real
            records.append(rec)

        output_c.writerecords(records)


def write_adjacency_matrix(
    links: str,
    nodes: str,
    weight_field: str,
    is_dual: bool,
    output: str,
) -> None:
    with fiona.open(nodes) as nodes_c, fiona.open(links) as links_c:
        edge_set = get_edge_set(links_c, nodes_c)
        size = len(nodes_c)
        matrix = get_adjacency_matrix(edge_set, weight_field, size, is_dual)
        hb_write(output, matrix)


def preprocess_network(
    links_collection: Collection,
    nodes_collection: Collection,
    link_id_field: str,
    node_id_field: str,
    link_lanenumber_field: str,
) -> Iterator[Dict[str, Any]]:
    for link_rec, (begin_rec, end_rec) in get_edge_set(links_collection,
                                                       nodes_collection):
        rec = link_rec.copy()
        rec['properties']['REF_IN_ID'] = str(
            begin_rec['properties'][node_id_field])
        rec['properties']['NREF_IN_ID'] = str(
            end_rec['properties'][node_id_field])
        rec['properties']['LINK_ID'] = str(
            link_rec['properties'][link_id_field])

        rec['properties']['DIR_TRAVEL'] = DIR_TRAVEL_DICT[rec['properties']
                                                          ['DIR']]
        if rec['properties'][link_lanenumber_field] is not None and int(
                rec['properties'][link_lanenumber_field]) > 10:
            rec['properties'][link_lanenumber_field] = 10

        yield rec


def write_preprocessed_network(
    links: str,
    nodes: str,
    output: str,
    driver: str,
    link_id_field: str,
    node_id_field: str,
    link_lanenumber_field: str,
) -> None:
    with fiona.open(links) as links_c:
        meta = links_c.meta.copy()
        meta['schema'] = network_schema(links_c.schema)
        meta['driver'] = driver

    with fiona.open(output, 'w', **meta) as output_c,\
            fiona.open(nodes) as nodes_c,\
            fiona.open(links) as links_c:
        output_c.writerecords(
            preprocess_network(links_c, nodes_c, link_id_field, node_id_field,
                               link_lanenumber_field))


def preprocess_taz(taz_collection: Collection,
                   links_collection: Collection) -> Iterator[Dict[str, Any]]:
    for link_rec in links_collection:
        link_shape = shapely.geometry.shape(link_rec['geometry'])
        for taz_rec in taz_collection:
            taz_shape = shapely.geometry.shape(taz_rec['geometry'])

            if taz_shape.contains(link_shape):
                link_rec['properties']['TAZ_ID'] = taz_rec['properties']['ID']
                break
        yield link_rec


def convert_shape_to_network(
    network_shapefile_prefix: str,
    link_id_field: str,
    link_speed_field: str,
    link_lanenumber_field: str,
    link_name_field: str,
    no_internal_links: bool,
    output_network: str,
    sumo_home: str,
) -> None:
    command = [
        "netconvert",
        "--shapefile-prefix",
        network_shapefile_prefix,
        "--shapefile.street-id",
        link_id_field,
        "--shapefile.speed",
        link_speed_field,
        "--shapefile.laneNumber",
        link_lanenumber_field,
        "--shapefile.traditional-axis-mapping",
        "true",
        "--output-file",
        output_network,
    ]

    if link_name_field:
        command.extend([
            "--shapefile.name",
            link_name_field,
        ])

    if no_internal_links:
        command.extend(["--no-internal-links", "true"])

    environ = os.environ.copy()
    environ.update(SUMO_HOME=sumo_home)
    subprocess.run(
        command,
        env=environ,
        capture_output=True,
        check=True,
    )


def convert_shape_to_taz(
    taz: str,
    taz_id_field: str,
    output_network: str,
    output_polygons: str,
    sumo_home: str,
) -> None:
    command = [
        "polyconvert",
        "--shapefile-prefixes",
        taz,
        "--shapefile.id-column",
        taz_id_field,
        "--shapefile.traditional-axis-mapping",
        "true",
        "--proj.utm",
        "true",
        "--net-file",
        output_network,
        "--output-file",
        output_polygons,
    ]
    environ = os.environ.copy()
    environ.update(SUMO_HOME=sumo_home)
    subprocess.run(
        command,
        env=environ,
    )


def edges_in_districts(
    output_network: str,
    output_polygons: str,
    output_district: str,
    sumo_home: str,
) -> None:
    command = [
        "python",
        "/usr/share/sumo/tools/edgesInDistricts.py",
        "--net-file",
        output_network,
        "--taz-files",
        output_polygons,
        "--output",
        output_district,
    ]
    environ = os.environ.copy()
    environ.update(SUMO_HOME=sumo_home)
    subprocess.run(
        command,
        env=environ,
    )


def omx_to_vissum_matrix(omx_file: str, output_dir: str) -> None:
    with omx.open_file(omx_file) as omx_matrix:
        mapping_title = omx_matrix.list_mappings()[0]
        map_entries = omx_matrix.map_entries(mapping_title)
        os.makedirs(output_dir)
        for matrix in omx_matrix:
            with open(os.path.join(output_dir, matrix.name + '.txt'),
                      'w') as output_file:
                print('$OR;D2', file=output_file)
                print('* From-Time  To-Time', file=output_file)
                print('0.00 1.00', file=output_file)
                print('* Factor', file=output_file)
                print('1.00', file=output_file)
                print('* Generated in pcardenasb.com', file=output_file)

                for i in range(matrix.shape[0]):
                    for j in range(matrix.shape[1]):
                        print(
                            "{:16d}{:16d}{:16.6f}".format(
                                map_entries[i],
                                map_entries[j],
                                matrix[i, j],
                            ),
                            file=output_file,
                        )


def patch_sumo_network(output_network: str, plain_output_prefix: str,
                       heuristic: str) -> None:
    netconvert = sumolib.checkBinary('netconvert')
    subprocess.run([
        netconvert,
        '--sumo-net-file',
        output_network,
        '--plain-output-prefix',
        plain_output_prefix,
    ])

    # Create all connections via heuristic
    net = sumolib.net.readNet(output_network)
    connections = ET.Element('connections')
    for node in net.getNodes():
        for inedge, outedge in itertools.product(node.getIncoming(),
                                                 node.getOutgoing()):
            in_lanes = inedge.getLaneNumber()
            out_lanes = outedge.getLaneNumber()

            lane_connections: Iterator[Tuple[int, int]]
            if heuristic == 'all_edges':
                lane_connections = ((from_lane,
                                     int(from_lane * out_lanes / in_lanes))
                                    for from_lane in range(in_lanes))
            elif heuristic == 'all_lanes':
                lane_connections = itertools.product(range(in_lanes),
                                                     range(out_lanes))

            for from_lane, to_lane in lane_connections:
                connection = ET.SubElement(connections, 'connection')
                connection.attrib['from'] = inedge.getID()
                connection.attrib['to'] = outedge.getID()
                connection.attrib['fromLane'] = str(from_lane)
                connection.attrib['toLane'] = str(to_lane)

    tree = ET.ElementTree(connections)
    if sys.version_info >= (3, 9):
        ET.indent(tree)
    tree.write(f'{plain_output_prefix}.con.xml')

    # Change most nodes to be unregulated
    tree = ET.parse(f'{plain_output_prefix}.nod.xml')
    root = tree.getroot()
    for node in root.iterfind("./node"):
        node.attrib['type'] = 'unregulated'

    if sys.version_info >= (3, 9):
        ET.indent(tree)
    tree.write(f'{plain_output_prefix}.nod.xml')

    # Aggregate
    subprocess.run([
        netconvert,
        '--node-files',
        f'{plain_output_prefix}.nod.xml',
        '--edge-files',
        f'{plain_output_prefix}.edg.xml',
        '--connection-files',
        f'{plain_output_prefix}.con.xml',
        '--output-file',
        output_network,
    ])


def read_tntp(path: str) -> pd.DataFrame:
    df = pd.read_csv(path, sep='\t', comment='<')

    if ';' in df:
        df.drop([';'], axis=1, inplace=True)
    if '~' in df:
        df.drop(['~'], axis=1, inplace=True)

    return df


TNTP_NODE_SCHEMA = {
    'properties': {
        'ID': 'str',
    },
    'geometry': 'Point',
}


def tntp_node_to_collection(df_node: pd.DataFrame) -> Collection:
    for _, row in df_node.iterrows():
        rec = {
            'properties': {
                'ID': row['Node'],
            },
            'geometry': {
                'type': 'Point',
                'coordinates': [row['X'], row['Y']],
            },
        }
        yield rec


TNTP_NET_SCHEMA = {
    'properties': {
        'ID': 'str',
        'REF_IN_ID': 'str',
        'NREF_IN_ID': 'str',
        'NOLANES': 'int',
        'capacity': 'float',
        'length': 'float',
        'free_flow': 'float',
        'b': 'float',
        'power': 'float',
        'speed': 'float',
        'toll': 'float',
        'link_type': 'int',
    },
    'geometry': 'LineString',
}


def tntp_net_to_collection(df_net: pd.DataFrame,
                           df_node: pd.DataFrame) -> Collection:
    for i, link_row in df_net.iterrows():
        init_node_id = link_row['init_node']
        term_node_id = link_row['term_node']

        init_node_row = df_node[df_node['Node'] == init_node_id].iloc[0]
        term_node_row = df_node[df_node['Node'] == term_node_id].iloc[0]

        rec = {
            'properties': {
                'ID': str(i),
                'REF_IN_ID': link_row['init_node'],
                'NREF_IN_ID': link_row['term_node'],
                'NOLANES': 5,
                'capacity': link_row['capacity'],
                'length': link_row['length'],
                'free_flow': link_row['free_flow_time'],
                'b': link_row['b'],
                'power': link_row['power'],
                'speed': 60,
                'toll': link_row['toll'],
                'link_type': link_row['link_type'],
            },
            'geometry': {
                'type':
                'LineString',
                'coordinates': [
                    [init_node_row['X'], init_node_row['Y']],
                    [term_node_row['X'], term_node_row['Y']],
                ],
            },
        }
        yield rec


def read_tntp_trips(
        matfile: str
) -> Tuple[npt.NDArray[np.float64], npt.NDArray[np.float64]]:
    '''Function to import OMX matrices'''
    with open(matfile) as f:
        all_rows = f.read()

    blocks = all_rows.split('Origin')[1:]
    matrix = {}
    for block in blocks:
        block_split = block.split('\n')
        dests = block_split[1:]
        orig = int(block_split[0])

        d = [
            eval('{' + a.replace(';', ',').replace(' ', '') + '}')
            for a in dests
        ]
        destinations: Dict[int, int] = {}
        for i in d:
            destinations = {**destinations, **i}
        matrix[orig] = destinations
    zones = max(matrix.keys())
    mat = np.zeros((zones, zones))
    for i in range(zones):
        for j in range(zones):
            # We map values to a index i-1, as Numpy is base 0
            mat[i, j] = matrix.get(i + 1, {}).get(j + 1, 0)

    indices = np.arange(zones) + 1

    return mat, indices


def get_taz(net_path: str) -> ET.ElementTree:
    xml_path = os.path.join(os.path.dirname(__file__), 'districts.taz.xml')
    tree = ET.parse(xml_path)
    root = tree.getroot()

    net = sumolib.net.readNet(net_path)
    for node in net.getNodes():
        taz = ET.SubElement(root, 'taz')
        taz.set('id', str(node.getID()))
        for out_edge in node.getOutgoing():
            taz_source = ET.SubElement(taz, 'tazSource')
            taz_source.set('id', str(out_edge.getID()))
            taz_source.set('weight', str(1))
        for in_edge in node.getOutgoing():
            taz_sink = ET.SubElement(taz, 'tazSink')
            taz_sink.set('id', str(in_edge.getID()))
            taz_sink.set('weight', str(1))

    return tree
