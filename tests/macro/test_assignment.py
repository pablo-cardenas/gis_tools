from gis_tools.macro.assignment import bfw, bpr
import numpy as np
import pandas as pd


def test_bfw():
    df_links = pd.DataFrame({
        'init_fid': [0, 0, 1, 2],
        'term_fid': [1, 2, 3, 3],
        'fftime': [15, 20, 0, 0],
        'capacity': [1000, 3000, 2000, 2000],
        'alpha': 0.15,
        'beta': 4,
        'preload': 500,
    })
    num_nodes = 4
    matrix = np.array([[0, 8000], [0, 0]])
    centroids_fid = [0, 3]
    order = 2
    num_iter = 20

    flows = bfw(
        df_links,
        num_nodes,
        matrix,
        centroids_fid,
        order,
        num_iter,
    )

    assert isinstance(flows, np.ndarray)


def test_bfw2():
    df_links = pd.DataFrame({
        'init_fid': [0, 1, 0, 2, 0, 3, 0, 4, 0, 5],
        'term_fid': [1, 6, 2, 6, 3, 6, 4, 6, 5, 6],
        'fftime': [20, 0, 21, 0, 22, 0, 23, 0, 24, 0],
        'capacity': 1000,
        'alpha': 0.15,
        'beta': 4,
        'preload': 500,
    })
    num_nodes = 7
    matrix = np.array([[0, 8000], [0, 0]])
    centroids_fid = [0, 6]
    order = 1
    num_iter = 30

    flows = bfw(
        df_links,
        num_nodes,
        matrix,
        centroids_fid,
        order,
        num_iter,
    )

    assert isinstance(flows, np.ndarray)
    assert isinstance(bpr(df_links, flows), pd.Series)
