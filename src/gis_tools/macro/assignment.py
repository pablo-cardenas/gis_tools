from typing import Sequence, Tuple, Dict, List

from numpy.linalg import solve, LinAlgError
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import dijkstra
import fiona
import numpy as np
import numpy.typing as npt
import openmatrix as omx
import pandas as pd

from gis_tools.sumo.utils import get_edge_set


def bpr(df_links: pd.DataFrame, flows: pd.Series) -> pd.Series:
    fftime = df_links['fftime']
    capacity = df_links['capacity']
    alpha = df_links['alpha']
    beta = df_links['beta']
    preload = df_links['preload']
    return fftime * (1 + alpha * ((flows + preload) / capacity)**beta)


def objective_bpr(df_links: pd.DataFrame, flows: pd.DataFrame) -> np.float64:
    fftime = df_links['fftime']
    capacity = df_links['capacity']
    alpha = df_links['alpha']
    beta = df_links['beta']
    preload = df_links['preload']
    ret = np.sum(fftime *
                 (flows + alpha * capacity *
                  ((flows + preload) / capacity)**(beta + 1) / (beta + 1)))

    assert isinstance(ret, np.float64)

    return ret


def all_or_nothing(
    df_links: pd.DataFrame,
    fftimes: pd.Series,
    num_nodes: int,
    matrix: npt.NDArray[np.float64],
    centroids_fid: List[int],
    links_from_nodes: Dict[Tuple[int, int], int],
) -> npt.NDArray[np.float64]:
    num_links = len(df_links)

    graph = csr_matrix((fftimes, (df_links['init_fid'], df_links['term_fid'])),
                       shape=(num_nodes, num_nodes))
    dist_matrix, predecessors = dijkstra(csgraph=graph,
                                         directed=True,
                                         indices=centroids_fid,
                                         return_predecessors=True)

    # flows_dis = np.zeros((*matrix.shape, num_links))
    flows_agg = np.zeros(num_links)
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            origin_fid = centroids_fid[i]
            destination_fid = centroids_fid[j]
            trips = matrix[i, j]

            term_node = destination_fid
            init_node = predecessors[i, term_node]

            if init_node == -9999 and trips == 0:
                continue

            while term_node != origin_fid:
                assert init_node != -9999

                link = links_from_nodes[(init_node, term_node)]

                # flows_dis[i, j, link] = trips
                flows_agg[link] += trips

                term_node = init_node
                init_node = predecessors[i, term_node]

    # return flows_dis
    return flows_agg


def conjugated_sight(df_links: pd.DataFrame, x: List[npt.NDArray[np.float64]],
                     s: List[npt.NDArray[np.float64]],
                     order: int) -> npt.NDArray[np.float64]:
    x_arr = np.array(x[-order - 1:])
    s_arr = np.array(s[-order - 1:])

    fftime = df_links['fftime']
    capacity = df_links['capacity']
    alpha = df_links['alpha']
    beta = df_links['beta']
    preload = df_links['preload']
    hessian = fftime * alpha * beta * \
        ((x_arr[-1]+preload) / capacity)**(beta - 1) / capacity
    hessian = hessian.to_numpy()

    matrix = np.einsum('ik,k,jk->ij', s_arr - x_arr, hessian,
                       s_arr - x_arr[-1])
    matrix[-1] = 1
    vector = np.array([0] * order + [1])

    # print(f'{s=}')
    # print(f'{x=}')
    # print(f'{s-x=}')
    # print(f'{s-x[-1]=}')
    # print(f'{hessian=}')
    # print(f'{vector=}')
    # print(f'{matrix=}')

    return solve(matrix, vector)  # type: ignore


def line_search(df_links: pd.DataFrame, x: pd.Series, s: pd.Series) -> float:
    tao_inf = 0.0
    tao_sup = 1.0

    direction = s - x

    for _ in range(64):
        tao = (tao_inf + tao_sup) / 2
        flows = tao * s + (1 - tao) * x
        fftimes = bpr(df_links, flows)

        z_deriv = direction @ fftimes

        if z_deriv > 0:
            tao_sup = tao
        else:
            tao_inf = tao

    return tao


def bfw(
    df_links: pd.DataFrame,
    num_nodes: int,
    matrix: npt.NDArray[np.float64],
    centroids_fid: List[int],
    max_order: int,
    num_iter: int,
) -> npt.NDArray[np.float64]:
    ttimes = bpr(df_links, df_links['preload'])

    links_from_nodes = {}
    for i_link, s_link in df_links.iterrows():
        links_from_nodes[(s_link['init_fid'], s_link['term_fid'])] = i_link

    flows_dis = all_or_nothing(
        df_links,
        ttimes,
        num_nodes,
        matrix,
        centroids_fid,
        links_from_nodes,
    )
    flows_agg = flows_dis
    # flows_agg = flows_dis.sum((0, 1))

    x_agg = [flows_agg]
    # x_dis = [flows_dis]
    s_agg: List[npt.NDArray[np.float64]] = []
    # s_dis = []

    order = 0
    for i in range(num_iter):
        curr_order = min(max_order, order)

        # print(f'{i=}')
        ttimes = bpr(df_links, x_agg[-1])
        flows_dis = all_or_nothing(
            df_links,
            ttimes,
            num_nodes,
            matrix,
            centroids_fid,
            links_from_nodes,
        )
        flows_agg = flows_dis
        # flows_agg = flows_dis.sum((0, 1))
        s_agg.append(flows_agg)
        # s_dis.append(flows_dis)

        try:
            alpha = conjugated_sight(df_links, x_agg, s_agg, curr_order)
        except LinAlgError:
            print('Using simple FW (singular matrix)')
            order = 0
        else:
            delta = 1e-2
            # print(f'{alpha=}')

            if alpha[-1] < delta:
                print('Using simple FW')
                order = 0
            else:
                alpha /= alpha[-1]
                alpha[alpha < 0] = 0
                alpha[:-1][alpha[:-1] > 1 - delta] = 1 - delta
                alpha /= alpha.sum()

                # print(f'{alpha=}')

                s_agg[-1] = alpha @ s_agg[-curr_order - 1:]
                # s_dis[-1] = np.einsum(
                #     'i,ijkl',
                #     alpha,
                #     s_dis[-curr_order - 1:],
                # )

        tao = line_search(df_links, x_agg[-1], s_agg[-1])
        flows_agg = tao * s_agg[-1] + (1 - tao) * x_agg[-1]  # type: ignore
        # flows_dis = tao * s_dis[-1] + (1 - tao) * x_dis[-1]

        x_agg.append(flows_agg)
        # x_dis.append(flows_dis)

        # print(f'{x_agg[-1]=}')
        # print(f'{s_agg[-1]=}')
        ttimes = bpr(df_links, x_agg[-1]).to_numpy()
        # print(f'{ttimes=}')
        print(objective_bpr(df_links, x_agg[-1]))

        order += 1

    # return x_dis[-1]
    return x_agg[-1]


def assignment_wrapper(
    links_shape: str,
    nodes_shape: str,
    matrix_omx: str,
    link_field: Dict['str', 'str'],
    node_field: Dict['str', 'str'],
    matrix_names: Sequence[Tuple[str, int]],
    output_file: str,
    order: int,
    num_iter: int,
) -> None:
    edge_set = list(
        get_edge_set(fiona.open(links_shape), fiona.open(nodes_shape)))
    num_nodes = len(fiona.open(nodes_shape))

    nodes_fid_from_id = {}
    with fiona.open(nodes_shape) as nodes_collection:
        for nodes_rec in nodes_collection:
            node_fid = int(nodes_rec['id'])
            node_id = nodes_rec['properties'][node_field['node_id']]
            nodes_fid_from_id[node_id] = node_fid

    links = []
    for i_link, (link_rec, (begin_rec, end_rec)) in enumerate(edge_set):
        link_id = link_rec['properties'][link_field['link_id']]
        link_dir = link_rec['properties']['DIR']
        init_fid = int(begin_rec['id'])
        term_fid = int(end_rec['id'])
        link_fid = int(link_rec['id'])
        ab_fftime = link_rec['properties'][link_field['AB_fftime']]
        ab_capacity = link_rec['properties'][link_field['AB_capacity']]
        ab_alpha = link_rec['properties'][link_field['AB_alpha']]
        ab_beta = link_rec['properties'][link_field['AB_beta']]
        ab_preload = link_rec['properties'][link_field['AB_preload']]
        ba_fftime = link_rec['properties'][link_field['BA_fftime']]
        ba_capacity = link_rec['properties'][link_field['BA_capacity']]
        ba_alpha = link_rec['properties'][link_field['BA_alpha']]
        ba_beta = link_rec['properties'][link_field['BA_beta']]
        ba_preload = link_rec['properties'][link_field['BA_preload']]

        if ab_fftime is None:
            ab_fftime = link_field['fftime']
        if ba_fftime is None:
            ba_fftime = link_field['fftime']
        if ab_capacity is None:
            ab_capacity = link_field['capacity']
        if ba_capacity is None:
            ba_capacity = link_field['capacity']
        if ab_preload is None:
            ab_preload = link_field['preload']
        if ba_preload is None:
            ba_preload = link_field['preload']
        if ab_alpha is None:
            ab_alpha = link_field['alpha']
        if ba_alpha is None:
            ba_alpha = link_field['alpha']
        if ab_beta is None:
            ab_beta = link_field['beta']
        if ba_beta is None:
            ba_beta = link_field['beta']

        if link_dir == 0:
            links.append({
                'link_id': link_id,
                'link_fid': link_fid,
                'direction': 1,
                'init_fid': init_fid,
                'term_fid': term_fid,
                'fftime': ab_fftime,
                'capacity': ab_capacity,
                'alpha': ab_alpha,
                'beta': ab_beta,
                'preload': ab_preload,
            })

            links.append({
                'link_id': link_id,
                'link_fid': link_fid,
                'direction': -1,
                'init_fid': term_fid,
                'term_fid': init_fid,
                'fftime': ba_fftime,
                'capacity': ba_capacity,
                'alpha': ba_alpha,
                'beta': ba_beta,
                'preload': ba_preload,
            })

        elif link_dir == 1:
            links.append({
                'link_id': link_id,
                'link_fid': link_fid,
                'direction': 1,
                'init_fid': init_fid,
                'term_fid': term_fid,
                'fftime': ab_fftime,
                'capacity': ab_capacity,
                'alpha': ab_alpha,
                'beta': ab_beta,
                'preload': ab_preload,
            })

        elif link_dir == -1:
            links.append({
                'link_id': link_id,
                'link_fid': link_fid,
                'direction': -1,
                'init_fid': term_fid,
                'term_fid': init_fid,
                'fftime': ab_fftime,
                'capacity': ab_capacity,
                'alpha': ab_alpha,
                'beta': ab_beta,
                'preload': ab_preload,
            })

    df_links = pd.DataFrame(links)
    num_links = len(df_links)

    with omx.open_file(matrix_omx) as omx_file:
        mapping_title = omx_file.list_mappings()[0]
        centroids = omx_file.map_entries(mapping_title)
        centroids_fid = [nodes_fid_from_id[node_id] for node_id in centroids]

        matrix = np.zeros(next(iter(omx_file)).shape)
        for matrix_name, pce in matrix_names:
            matrix += pce * omx_file[matrix_name].read()

    flows_dis = bfw(df_links, num_nodes, matrix, centroids_fid, order,
                    num_iter)
    flows_agg = flows_dis
    # flows_agg = flows_dis.sum((0, 1))

    columns = ['link_id', 'AB_flow', 'BA_flow', 'AB_ttime', 'BA_ttime']
    for i_origin in range(flows_dis.shape[0]):
        for i_destination in range(flows_dis.shape[1]):
            columns.extend([
                'AB{:03d}-{:03d}'.format(centroids[i_origin],
                                         centroids[i_destination]),
                'BA{:03d}-{:03d}'.format(centroids[i_origin],
                                         centroids[i_destination]),
            ])

    df = pd.DataFrame(columns=columns)
    ttimes = bpr(df_links, flows_agg).to_numpy()

    for i in range(num_links):
        direction = df_links['direction'].iat[i]
        link_fid = df_links['link_fid'].iat[i]

        df.loc[link_fid, 'link_id'] = df_links['link_id'].iat[i]
        if direction == 1:
            df.loc[link_fid, 'AB_flow'] = flows_agg[i]
            df.loc[link_fid, 'AB_ttime'] = ttimes[i]
            # for i_origin in range(flows_dis.shape[0]):
            #    for i_destination in range(flows_dis.shape[1]):
            #        colname = 'AB{:03d}-{:03d}'.format(
            #            centroids[i_origin], centroids[i_destination])
            #        df.loc[link_fid, colname] = flows_dis[i_origin,
            #                                             i_destination, i]

        elif direction == -1:
            df.loc[link_fid, 'BA_flow'] = flows_agg[i]
            df.loc[link_fid, 'BA_ttime'] = ttimes[i]
            # for i_origin in range(flows_dis.shape[0]):
            #    for i_destination in range(flows_dis.shape[1]):
            #        colname = 'BA{:03d}-{:03d}'.format(
            #            centroids[i_origin], centroids[i_destination])
            #        df.loc[link_fid, colname] = flows_dis[i_origin,
            #                                              i_destination, i]

    df.to_csv(output_file)
