import os

import pytest
import fiona


@pytest.fixture
def nodes_collection():
    path = os.path.join(os.path.dirname(__file__), 'data/transcad/nodes.json')
    with fiona.open(path) as collection:
        yield collection


@pytest.fixture
def links_collection():
    path = os.path.join(os.path.dirname(__file__), 'data/transcad/links.json')
    with fiona.open(path) as collection:
        yield collection


@pytest.fixture
def taz_collection():
    path = os.path.join(os.path.dirname(__file__), 'data/transcad/taz.json')
    with fiona.open(path) as collection:
        yield collection
