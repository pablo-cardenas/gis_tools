import json
import os

import fiona

from gis_tools.sumo.utils import (preprocess_network, network_schema,
                                  preprocess_taz, read_tntp,
                                  tntp_node_to_collection,
                                  tntp_net_to_collection)


def test_network_schema():
    link_schema = {
        'properties': {
            'ID': 'int:10',
            'DIR': 'int:10',
            'NOMBREVIA': 'str:60',
            'VEL2020': 'int:10',
            'LANE2020': 'int:10',
        },
        'geometry': 'LineString',
    }

    schema = network_schema(link_schema)

    assert isinstance(schema, dict)

    assert 'REF_IN_ID' in schema['properties']
    assert 'NREF_IN_ID' in schema['properties']
    assert 'LINK_ID' in schema['properties']

    assert fiona.prop_type(schema['properties']['REF_IN_ID']) == str
    assert fiona.prop_type(schema['properties']['NREF_IN_ID']) == str
    assert fiona.prop_type(schema['properties']['LINK_ID']) == str


def test_preprocess_network(links_collection, nodes_collection):
    edges_path = os.path.join(
        os.path.dirname(__file__),
        'data/transcad/edges.json',
    )
    edges = json.load(open(edges_path))

    for rec in preprocess_network(
            links_collection,
            nodes_collection,
            link_id_field="ID",
            node_id_field="ID",
            link_lanenumber_field="AB_CARRIL",
    ):
        assert (edges[rec['properties']['LINK_ID']].items() <=
                rec['properties'].items())


def test_preprocess_taz(taz_collection, links_collection):
    count = 0
    for rec in preprocess_taz(taz_collection, links_collection):
        if int(rec['properties']['ID']) == 1:
            count += 1
            assert 'TAZ_ID' in rec['properties']
            assert rec['properties']['TAZ_ID'] == 1
        if int(rec['properties']['ID']) == 7:
            count += 1
            assert 'TAZ_ID' in rec['properties']
            assert rec['properties']['TAZ_ID'] == 2
    assert count == 2


def test_read_tntp():
    pass


def test_tntp_to_collection():
    df = read_tntp(
        os.path.join(os.path.dirname(__file__),
                     'data/tntp/SiouxFalls_node.tntp'))
    for rec in tntp_node_to_collection(df):
        assert float(rec['geometry']['coordinates'][0])
        assert float(rec['geometry']['coordinates'][1])


def test_tntp_net_to_collection():
    df_net = read_tntp(
        os.path.join(os.path.dirname(__file__),
                     'data/tntp/SiouxFalls_net.tntp'))
    df_node = read_tntp(
        os.path.join(os.path.dirname(__file__),
                     'data/tntp/SiouxFalls_node.tntp'))

    for rec in tntp_net_to_collection(df_net, df_node):
        pass
