from jinja2 import Environment, PackageLoader, select_autoescape
import concurrent.futures
import sumolib
import subprocess
import os
import re
import tempfile
from itertools import product
import json
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from .models import Scenario, Interval
from pathlib import Path

sumo = sumolib.checkBinary('sumo')

tripinfo_folder = Path("tripinfo")
tripinfo_folder.mkdir(exist_ok=True)
edgedata_folder = Path("edgedata")
edgedata_folder.mkdir(exist_ok=True)
scenarios_folder = Path("scenarios")
scenarios_folder.mkdir(exist_ok=True)

env = Environment(
    loader=PackageLoader('gis_tools.sumo.scenarios'),
    autoescape=select_autoescape(),
    trim_blocks=True,
    lstrip_blocks=True,
)
template = env.get_template('rerouter.add.xml')


def run_sumo(arg):
    interval, scenario = arg
    prefix = f'{scenario.id}_{interval.id}'

    with open("done.txt") as f:
        if f"{prefix}\n" in f.readlines():
            return

    command = [sumo, '--configuration-file', 'net.sumocfg']

    command.extend([
        '--tripinfo-output',
        tripinfo_folder / f'{prefix}.xml',
        '--additional-files',
        scenarios_folder / f'{prefix}.add.xml',
    ])

    subprocess.run(command, check=True)

    with open("done.txt", "a") as f:
        f.write(f"{prefix}\n")


engine = create_engine('sqlite:///data.db')
with Session(engine) as session:
    intervals = session.query(Interval).all()
    scenarios = session.query(Scenario).all()

    arg = list(product(intervals, scenarios))

    for interval, scenario in arg:
        with open(scenarios_folder / f'{scenario.id}_{interval.id}.add.xml',
                  'w') as f:
            f.write(
                template.render(
                    scenario=scenario,
                    interval=interval,
                    edgedata=Path(
                        os.path.relpath(
                            edgedata_folder,
                            scenarios_folder,
                        )),
                ))
    input("Are you sure")

    with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
        executor.map(run_sumo, arg)
